const hour = document.querySelector('.hour-time'),
    minut = document.querySelector('.minute-time'),
    second = document.querySelector('.second-time');


function cutDownTime() {    
    let setHours = hour.innerHTML
    let setMinutes = minut.innerHTML
    let setSeconds = second.innerHTML
    
    let time = setInterval(() => {
        setSeconds--;
        second.innerHTML = setSeconds;
        if (setSeconds == 0){
          setMinutes -= 1;
          minut.innerHTML = setMinutes
          setSeconds = 60;
        }
        if (setMinutes == 0){
          setHours -= 1;
          hour.innerHTML = setHours
          setMinutes = 60;
        }
    }, 1000);
    if (setSeconds == 0 && setHours == 0 && setMinutes == 0) {
      clearInterval(time);
    }
}
cutDownTime()